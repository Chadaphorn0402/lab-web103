*** Settings ***
Library           SeleniumLibrary
Library           Collections
Library           BuiltIn
Library           JSONLibrary
Library           RequestsLibrary

*** Test Cases ***
LIST_USERS
    Create Session    url    https://reqres.in    verify=True
    ${resp}    Get Request    url    /api/users?page=2
    Log To Console    ${EMPTY}
    Log To Console    ${resp}
    Log To Console    ${resp.json()}
    Status Should Be    200    ${resp}
    ${data_list}    Get Length    ${resp.json()["data"]}
    ${name_list}    Create List    Michael    Lindsay    Tobias    Byron    George    Rachel
    FOR    ${i}    IN RANGE    0    ${data_list}
        Should Be Equal As Strings    ${name_list[${i}]}    ${resp.json()["data"][${i}]["first_name"]}
    END

POST_CREATE
    Create Session    url    https://reqres.in    verify=True
    ${head}    Create Dictionary    content-type=application/json
    ${body}    Create Dictionary    name=morpheus    job=leader
    ${resp}    Post Request    url    /api/users    headers=${head}    data=${body}
    Log To Console    ${EMPTY}
    Log To Console    ${resp}
    Log To Console    ${resp.json()}
    Status Should Be    201    ${resp}
    Should Be Equal As Strings    morpheus    ${resp.json()["name"]}
    Should Be Equal As Strings    leader    ${resp.json()["job"]}
